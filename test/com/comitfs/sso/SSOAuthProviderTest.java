package com.comitfs.sso;

import com.comitfs.cas.sso.jwt.CASJWToken;

import java.io.UnsupportedEncodingException;

public class SSOAuthProviderTest {

    public static void main(String[] args) {
        try {
            String s = CASJWToken.generateToken("CAS", 2592000, "Test");
            System.out.println("Token: " + s);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
