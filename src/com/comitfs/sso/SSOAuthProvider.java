package com.comitfs.sso;


import com.comitfs.cas.sso.jwt.CASJWToken;
import org.apache.commons.lang3.StringUtils;
import org.jivesoftware.openfire.auth.AuthProvider;
import org.jivesoftware.openfire.auth.ConnectionException;
import org.jivesoftware.openfire.auth.InternalUnauthenticatedException;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SSOAuthProvider implements AuthProvider {

    private static final Logger Log = LoggerFactory.getLogger(SSOAuthProvider.class);


    public SSOAuthProvider() {
        super();
        String contextName = JiveGlobals.getProperty("cas.auth.siteminder.context.name", "/http-sso-bind/");
        if (contextName != null) {

            if (contextName.contains(",")) {
                String[] conStrings = contextName.split(",");
                for (String con : conStrings) {
                    AddSSOContext.getInstance().createSSOContextHandler(con);
                }
            } else {
                AddSSOContext.getInstance().createSSOContextHandler(contextName);
            }
        }
    }

    @Override
    public void authenticate(String userName, String password) throws UnauthorizedException {
        String user = userName.split("@")[0];
        if (StringUtils.isBlank(user)) {
            throw new UnauthorizedException();
        }

        if (JiveGlobals.getBooleanProperty("plugin.cas.rest.api.nonauth.enabled", false)) {
            return;
        }

        if (AddSSOContext.getInstance().getUserSessionMap().containsKey(user) && authenticateUser(user)) {
            return;
        }

        SSOAuthModel ssoAuthModel = getConfiguredSSOAuthModel();
        switch (ssoAuthModel) {
            case TOKEN:
                try {
                    CASJWToken.getInstance()
                            .validateToken(password, JiveGlobals.getProperty("cas.sso.auth.jwt.audience", "cas")
                                    , JiveGlobals.getProperty("cas.sso.auth.jwt.subject", "cas-sso"));
                    return;
                } catch (Exception e) {
                    Log.error("User " + userName + " authentication failed");
                    throw new UnauthorizedException();
                }
        }

        if (JiveGlobals.getBooleanProperty("plugin.cas.rest.api.sso.enabled", false)) {
            String userid = JiveGlobals.getProperty("plugin.cas.rest.api.local.session.user");
            if (!userid.equalsIgnoreCase(userName)) {
                throw new UnauthorizedException();
            }

            User usr;
            try {
                usr = UserManager.getInstance().getUser(userName.trim());
                if (!usr.getProperties().containsKey("restSecret")) {
                    Log.error("User " + userName + " authentication failed");
                    throw new UnauthorizedException();
                }

                String restSecret = usr.getProperties().get("restSecret");
                if (restSecret.equalsIgnoreCase(password)) {
                    return;
                } else {
                    Log.error("User " + userName + " authentication failed");
                }
            } catch (UserNotFoundException e) {
                Log.error("User " + userName + " is not registered in openfire");
            }
        }
        throw new UnauthorizedException();
    }

    private SSOAuthModel getConfiguredSSOAuthModel() throws UnauthorizedException {
        try {
            String ssoAuthModelStr = JiveGlobals.getProperty("cas.sso.auth.model", SSOAuthModel.PWD.name());
            return SSOAuthModel.valueOf(ssoAuthModelStr.toUpperCase());
        } catch (Exception e) {
        }
        throw new UnauthorizedException("invalid cas auth model");
    }

    private Boolean authenticateUser(String userName) {
        Boolean result = Boolean.FALSE;
        User user;
        if (JiveGlobals.getBooleanProperty("cas.auth.siteminder.cas.client.authorise", false)) {
            Log.info("**************" + "authenticating user : " + userName + " ***************** ");
            try {
                user = UserManager.getInstance().getUser(userName.trim());
                Log.info("**************" + "found user : " + userName + " ***************** ");
                List<Group> groups = new ArrayList<Group>(GroupManager.getInstance().getGroups(user));


                if (groups == null || groups.size() == 0) {
                    Log.info("**************" + "No groups found user : " + userName + " ***************** ");
                    return result;
                }

                for (Group group : groups) {
                    Log.info("**************" + "Group user : " + group.getName() + " ***************** ");
                    if (group.getName().equalsIgnoreCase(
                            JiveGlobals.getProperty("cas.auth.siteminder.cas.client.group", "itrader-dev"))) {
                        result = Boolean.TRUE;
                    }
                }
            } catch (UserNotFoundException e) {
                Log.error("User " + userName + " is not registered in openfire");
            } catch (Exception e) {
                Log.error("Error while authenticating ...", e);
            } catch (Throwable t) {
                Log.error("Error while authenticating ...", t);
            }
        }
        return result;
    }

    public void authenticate(String arg0, String arg1, String arg2)
            throws UnauthorizedException, ConnectionException, InternalUnauthenticatedException {
        // TODO Auto-generated method stub

    }

    @Override
    public String getPassword(String arg0) throws UserNotFoundException, UnsupportedOperationException {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isDigestSupported() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isPlainSupported() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isScramSupported() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public void setPassword(String arg0, String arg1) throws UserNotFoundException, UnsupportedOperationException {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean supportsPasswordRetrieval() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getSalt(String username) throws UnsupportedOperationException, UserNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getIterations(String username) throws UnsupportedOperationException, UserNotFoundException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getServerKey(String username) throws UnsupportedOperationException, UserNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getStoredKey(String username) throws UnsupportedOperationException, UserNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

}
