package com.comitfs.sso;

import org.apache.tomcat.InstanceManager;
import org.apache.tomcat.SimpleInstanceManager;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.plus.annotation.ContainerInitializer;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jivesoftware.openfire.http.HttpBindManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddSSOContext {

    public static final AddSSOContext addSSOContext = new AddSSOContext();
    private final Map<String, String> userSessionMap = new HashMap<String, String>();

    public static AddSSOContext getInstance() {
        return addSSOContext;
    }

    public Map<String, String> getUserSessionMap() {
        return userSessionMap;
    }

    public void createSSOContextHandler(String boshPath) {

        ServletContextHandler context = new ServletContextHandler(null, boshPath, ServletContextHandler.SESSIONS);
        final List<ContainerInitializer> initializers = new ArrayList<>();
        initializers.add(new ContainerInitializer(new JettyJasperInitializer(), null));
        context.setAttribute("org.eclipse.jetty.containerInitializers", initializers);
        context.setAttribute(InstanceManager.class.getName(), new SimpleInstanceManager());
        context.addServlet(new ServletHolder(new SSOApiServlet()), "/*");
        HttpBindManager.getInstance().addJettyHandler(context);
    }
}
