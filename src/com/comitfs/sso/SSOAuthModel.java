package com.comitfs.sso;

public enum SSOAuthModel {
    PWD,
    TOKEN
}
