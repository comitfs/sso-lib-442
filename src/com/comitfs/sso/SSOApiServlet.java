package com.comitfs.sso;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.AsyncContext;
import javax.servlet.ReadListener;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.QName;
import org.dom4j.io.XMPPPacketReader;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.http.BoshBindingError;
import org.jivesoftware.openfire.http.HttpBindBody;
import org.jivesoftware.openfire.http.HttpBindException;
import org.jivesoftware.openfire.http.HttpBindManager;
import org.jivesoftware.openfire.http.HttpBindServlet;
import org.jivesoftware.openfire.http.HttpConnection;
import org.jivesoftware.openfire.http.HttpConnectionClosedException;
import org.jivesoftware.openfire.http.HttpSession;
import org.jivesoftware.openfire.http.HttpSessionManager;
import org.jivesoftware.openfire.http.SessionEventDispatcher;
import org.jivesoftware.openfire.net.MXParser;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class SSOApiServlet extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger Log = LoggerFactory.getLogger(HttpBindServlet.class);

    private HttpSessionManager sessionManager;
    private HttpBindManager boshManager;
    

    private static XmlPullParserFactory factory;

    static {
        try {
            factory = XmlPullParserFactory.newInstance(MXParser.class.getName(), null);
        }
        catch (XmlPullParserException e) {
            Log.error("Error creating a parser factory", e);
        }
    }

    private ThreadLocal<XMPPPacketReader> localReader = new ThreadLocal<>();

    public SSOApiServlet() {
    }


    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        boshManager = HttpBindManager.getInstance();
        sessionManager = boshManager.getSessionManager();
        sessionManager.start();
    }


    @Override
    public void destroy() {
        super.destroy();
        sessionManager.stop();
    }

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// add CORS headers for all HTTP responses (errors, etc.)
        if (boshManager.isCORSEnabled())
        {
            if (boshManager.isAllOriginsAllowed()) {
                // Set the Access-Control-Allow-Origin header to * to allow all Origin to do the CORS
                response.setHeader("Access-Control-Allow-Origin", HttpBindManager.HTTP_BIND_CORS_ALLOW_ORIGIN_DEFAULT);
            } else {
                // Get the Origin header from the request and check if it is in the allowed Origin Map.
                // If it is allowed write it back to the Access-Control-Allow-Origin header of the respond.
                final String origin = request.getHeader("Origin");
                if (boshManager.isThisOriginAllowed(origin)) {
                    response.setHeader("Access-Control-Allow-Origin", origin);
                }
            }
            response.setHeader("Access-Control-Allow-Methods", HttpBindManager.HTTP_BIND_CORS_ALLOW_METHODS_DEFAULT);
            response.setHeader("Access-Control-Allow-Headers", HttpBindManager.HTTP_BIND_CORS_ALLOW_HEADERS_DEFAULT);
            response.setHeader("Access-Control-Max-Age", HttpBindManager.HTTP_BIND_CORS_MAX_AGE_DEFAULT);
        }
        super.service(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        final AsyncContext context = request.startAsync();

        boolean isScriptSyntaxEnabled = boshManager.isScriptSyntaxEnabled();
                
        if(!isScriptSyntaxEnabled) {
            sendLegacyError(context, BoshBindingError.itemNotFound);
            return;
        }

        String queryString = request.getQueryString();
        if (queryString == null || "".equals(queryString)) {
            sendLegacyError(context, BoshBindingError.badRequest);
            return;
        } else if ("isBoshAvailable".equals(queryString)) {
            response.setStatus(HttpServletResponse.SC_OK);
            context.complete();
            return;
        }
        queryString = URLDecoder.decode(queryString, "UTF-8");
        Log.info("request headers : " + request.getHeader("smuser"));
        processContent(context, queryString,"");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final AsyncContext context = request.startAsync();
        String headerName = JiveGlobals.getProperty("cas.auth.siteminder.header", "SMUSER");
//        response.addHeader("Access-Control-Allow-Origin", "*");
//        response.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
//        response.addHeader("Access-Control-Allow-Headers","*");
        String userName = request.getHeader(headerName);
        request.getInputStream().setReadListener(new ReadListenerImpl(context, userName));
    }

    protected void processContent(AsyncContext context, String content, String userName)
            throws IOException {
        final String remoteAddress = getRemoteAddress(context);

        // Parse document from the content.
//        Document document;
//        try {
//            document = getPacketReader().read(new StringReader(content), "UTF-8");
//            
//        } catch (Exception ex) {
//            Log.warn("Error parsing request data from [" + remoteAddress + "]", ex);
//            sendLegacyError(context, BoshBindingError.badRequest);
//            return;
//        }
        
        final HttpBindBody body;
        try {
            body = HttpBindBody.from( content );
        } catch (Exception ex) {
            Log.warn("Error parsing request data from [" + remoteAddress + "]", ex);
            sendLegacyError(context, BoshBindingError.badRequest);
            return;
        }
//        if (document == null) {
//            Log.info("The result of parsing request data from [" + remoteAddress + "] was a null-object.");
//            sendLegacyError(context, BoshBindingError.badRequest);
//            return;
//        }
//
//        final Element node = document.getRootElement();
//        if (node == null || !"body".equals(node.getName())) {
//            Log.info("Root element 'body' is missing from parsed request data from [" + remoteAddress + "]");
//            sendLegacyError(context, BoshBindingError.badRequest);
//            return;
//        }

        final long rid = body.getRid();
        if (rid <= 0) {
            Log.info("Root element 'body' does not contain a valid RID attribute value in parsed request data from [" + remoteAddress + "]");
            sendLegacyError(context, BoshBindingError.badRequest, "Body-element is missing a RID (Request ID) value, or the provided value is a non-positive integer.");
            return;
        }

        // Process the parsed document.
        final String sid = body.getSid();
        if (sid == null) {
            // When there's no Session ID, this should be a request to create a new session. If there's additional content,
            // something is wrong.
        	if (!body.isEmpty()) {
        		// invalid session request; missing sid
                Log.info("Root element 'body' does not contain a SID attribute value in parsed request data from [" + remoteAddress + "]");
                sendLegacyError(context, BoshBindingError.badRequest);
                return;
            }

            // We have a new session
            String sessionId = createNewSession(context, body);
            AddSSOContext.getInstance().getUserSessionMap().put(userName, sessionId);
        }
        else {
            // When there exists a Session ID, new data for an existing session is being provided.
        	if(userName != null) {
        		HttpBindBody alterNode = alterPacket(userName, body);
        		handleSessionRequest(context, alterNode);
        	}else {
        		
        		handleSessionRequest( context, body);
        	}
        	
        }
    }
    
    private Boolean autheticateUser(String userName) {
    	Boolean result = Boolean.FALSE;
    	 User user;
		try {
			user = UserManager.getInstance().getUser(userName.trim());
			List<Group> groups = new ArrayList<Group>(GroupManager.getInstance().getGroups(user));
			for(Group group : groups) {
				if(group.getName().equalsIgnoreCase(JiveGlobals.getProperty("sso.user.group.name", "ssouser"))) {
					result = Boolean.TRUE;		
				}
			}
		} catch (UserNotFoundException e) {
			Log.error("User "+ userName + " is not registered in openfire");
		}
         return result;
    }
    
    private HttpBindBody alterPacket(String userName, HttpBindBody body) {
    		Element returnNode = body.getDocument().getRootElement() ;
    		 String hostname = XMPPServer.getInstance().getServerInfo().getHostname();
             String domain = JiveGlobals.getProperty("xmpp.domain",hostname);
             String key = userName+"@"+domain+"\0"+userName+"\0"+userName;
             String encodeData= StringUtils.encodeBase64(key);
    		List<Element> elements = body.getDocument().getRootElement().elements();
            for (Element packet : elements) {
                try {
                	if(packet.getName().equals("auth")) {
                		packet.setText(encodeData);
                	}
                }
                catch (Exception e) {
                    Log.error("Client provided unknown packet type", e);
                }
            }
    		
    	
         return body;
    }

//    protected String createNewSession(AsyncContext context, HttpBindBody body)
//            throws IOException
//    {
//        final long rid = getLongAttribute(rootNode.attributeValue("rid"), -1);
//        String sid = null;
//        try {
//            final X509Certificate[] certificates = (X509Certificate[]) context.getRequest().getAttribute("javax.servlet.request.X509Certificate");
//            final HttpConnection connection = new HttpConnection(rid,  context);
//            final InetAddress address = InetAddress.getByName(context.getRequest().getRemoteAddr());
//            //connection.setSession(sessionManager.createSession(address, rootNode, connection));
//           HttpSession session = sessionManager.createSession(rootNode, connection);
//           sid = connection.getSession().getStreamID().getID();
////            if (JiveGlobals.getBooleanProperty("log.httpbind.enabled", false)) {
////                Log.info(new Date() + ": HTTP RECV(" + connection.getSession().getStreamID().getID() + "): " + rootNode.asXML());
////            }
//        }
//        catch (UnauthorizedException | HttpBindException e) {
//            // Server wasn't initialized yet.
//            sendLegacyError(context, BoshBindingError.internalServerError, "Server has not finished initialization." );
//        }
//        return sid;
//    }
    
    protected String createNewSession(AsyncContext context, HttpBindBody body)
            throws IOException
    {
        final long rid = body.getRid();
        String sid = null;
        try {
            final HttpConnection connection = new HttpConnection(rid, context);

            SessionEventDispatcher.dispatchEvent( null, SessionEventDispatcher.EventType.pre_session_created, connection, context );

           // connection.setSession(sessionManager.createSession(body, connection));
            HttpSession session = sessionManager.createSession(body, connection);
            sid = connection.getSession().getStreamID().getID();
            if (JiveGlobals.getBooleanProperty("log.httpbind.enabled", false)) {
                Log.info("HTTP RECV(" + connection.getSession().getStreamID().getID() + "): " + body.asXML());
            }

            SessionEventDispatcher.dispatchEvent( connection.getSession(), SessionEventDispatcher.EventType.post_session_created, connection, context );
        }
        catch (Exception e) {
            // Server wasn't initialized yet.
            sendLegacyError(context, BoshBindingError.internalServerError, "Server has not finished initialization." );
        }
        return sid;
    }

    private void handleSessionRequest(AsyncContext context, HttpBindBody body)
            throws IOException
    {
        final String sid = body.getSid();
        if (JiveGlobals.getBooleanProperty("log.httpbind.enabled", false)) {
            Log.info("HTTP RECV(" + sid + "): " + body.asXML());
        }

        HttpSession session = sessionManager.getSession(sid);
        if (session == null) {
            if (Log.isDebugEnabled()) {
                Log.debug("Client provided invalid session: " + sid + ". [" +
                    context.getRequest().getRemoteAddr() + "]");
            }
            sendLegacyError(context, BoshBindingError.itemNotFound, "Invalid SID value.");
            return;
        }

        synchronized (session) {
            try {
                session.forwardRequest(body, context);
            }
            catch (HttpBindException e) {
                sendError(session, context, e.getBindingError());
            }
            catch (HttpConnectionClosedException nc) {
                Log.error("Error sending packet to client.", nc);
                context.complete();
            }
        }
    }


    private XMPPPacketReader getPacketReader()
    {
        // Reader is associated with a new XMPPPacketReader
        XMPPPacketReader reader = localReader.get();
        if (reader == null) {
            reader = new XMPPPacketReader();
            reader.setXPPFactory(factory);
            localReader.set(reader);
        }
        return reader;
    }

    public static void respond(HttpSession session, AsyncContext context, String content, boolean async) throws IOException
    {
        final HttpServletResponse response = ((HttpServletResponse) context.getResponse());
        final HttpServletRequest request = ((HttpServletRequest) context.getRequest());

        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("GET".equals(request.getMethod()) ? "text/javascript" : "text/xml");
        response.setCharacterEncoding("UTF-8");

        if ("GET".equals(request.getMethod())) {
            if (JiveGlobals.getBooleanProperty("xmpp.httpbind.client.no-cache.enabled", true)) {
                // Prevent caching of responses
                response.addHeader("Cache-Control", "no-store");
                response.addHeader("Cache-Control", "no-cache");
                response.addHeader("Pragma", "no-cache");
            }
            content = "_BOSH_(\"" + StringEscapeUtils.escapeEcmaScript(content) + "\")";
        }
        
        if (JiveGlobals.getBooleanProperty("log.httpbind.enabled", false)) {
            System.out.println(new Date() + ": HTTP SENT(" + session.getStreamID().getID() + "): " + content);
        }

        final byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);
        // BOSH communication should not use Chunked encoding.
        // This is prevented by explicitly setting the Content-Length header.
        response.setContentLength(byteContent.length);

        if (async) {
            response.getOutputStream().setWriteListener(new WriteListenerImpl(context, byteContent));
        } else {
            context.getResponse().getOutputStream().write(byteContent);
            context.getResponse().getOutputStream().flush();
            context.complete();
        }
    }

    private void sendError(HttpSession session, AsyncContext context, BoshBindingError bindingError)
            throws IOException
    {
        if (JiveGlobals.getBooleanProperty("log.httpbind.enabled", false)) {
            System.out.println(new Date() + ": HTTP ERR(" + session.getStreamID().getID() + "): " + bindingError.getErrorType().getType() + ", " + bindingError.getCondition() + ".");
        }
        try {
            if ((session.getMajorVersion() == 1 && session.getMinorVersion() >= 6) || session.getMajorVersion() > 1)
            {
                final String errorBody = createErrorBody(bindingError.getErrorType().getType(), bindingError.getCondition());
                respond(session, context, errorBody, true);
            } else {
                sendLegacyError(context, bindingError);
            }
        }
        finally {
            if (bindingError.getErrorType() == BoshBindingError.Type.terminate) {
                session.close();
            }
        }
    }

    protected static void sendLegacyError(AsyncContext context, BoshBindingError error, String message)
            throws IOException
    {
        final HttpServletResponse response = (HttpServletResponse) context.getResponse();
        if (message == null || message.trim().length() == 0) {
            response.sendError(error.getLegacyErrorCode());
        } else {
            response.sendError(error.getLegacyErrorCode(), message);
        }
        context.complete();
    }

    protected static void sendLegacyError(AsyncContext context, BoshBindingError error)
            throws IOException
    {
        sendLegacyError(context, error, null);
    }

    protected static String createErrorBody(String type, String condition) {
        final Element body = DocumentHelper.createElement( QName.get( "body", "http://jabber.org/protocol/httpbind" ) );
        body.addAttribute("type", type);
        body.addAttribute("condition", condition);
        return body.asXML();
    }

    protected static long getLongAttribute(String value, long defaultValue) {
        if (value == null || "".equals(value)) {
            return defaultValue;
        }
        try {
            return Long.valueOf(value);
        }
        catch (Exception ex) {
            return defaultValue;
        }
    }

    protected static int getIntAttribute(String value, int defaultValue) {
        if (value == null || "".equals(value)) {
            return defaultValue;
        }
        try {
            return Integer.valueOf(value);
        }
        catch (Exception ex) {
            return defaultValue;
        }
    }

    protected static String getRemoteAddress(AsyncContext context)
    {
        String remoteAddress = null;
        if (context.getRequest() != null && context.getRequest().getRemoteAddr() != null) {
            remoteAddress = context.getRequest().getRemoteAddr();
        }

        if (remoteAddress == null || remoteAddress.trim().length() == 0) {
            remoteAddress = "<UNKNOWN ADDRESS>";
        }

        return remoteAddress;
    }

    class ReadListenerImpl implements ReadListener {

        private final AsyncContext context;
        private final ByteArrayOutputStream outStream = new ByteArrayOutputStream(1024);
        private final String remoteAddress;
        private  String userName;

        ReadListenerImpl(AsyncContext context, String userName) {
            this.context = context;
            this.remoteAddress = getRemoteAddress(context);
            this.userName = userName;
        }

        @Override
        public void onDataAvailable() throws IOException {
            Log.trace("Data is available to be read from [" + remoteAddress + "]");

            final ServletInputStream inputStream = context.getRequest().getInputStream();

            byte b[] = new byte[1024];
            int length;
            while (inputStream.isReady() && (length = inputStream.read(b)) != -1) {
                outStream.write(b, 0, length);
            }
        }

        @Override
        public void onAllDataRead() throws IOException {
            Log.trace("All data has been read from [" + remoteAddress + "]");
            processContent(context, outStream.toString(StandardCharsets.UTF_8.name()), userName);
        }

        @Override
        public void onError(Throwable throwable) {
            Log.warn("Error reading request data from [" + remoteAddress + "]", throwable);
            try {
                sendLegacyError(context, BoshBindingError.badRequest);
            } catch (IOException ex) {
                Log.debug("Error while sending an error to ["+remoteAddress +"] in response to an earlier data-read failure.", ex);
            }
        }
    }

//    private static class WriteListenerImpl implements WriteListener {
//
//        private final AsyncContext context;
//        private final byte[] data;
//        private final String remoteAddress;
//        private volatile boolean written;
//
//        public WriteListenerImpl(AsyncContext context, byte[] data) {
//            this.context = context;
//            this.data = data;
//            this.remoteAddress = getRemoteAddress(context);
//        }
//
//        @Override
//        public void onWritePossible() throws IOException {
//            // This method may be invoked multiple times and by different threads, e.g. when writing large byte arrays.
//            Log.trace("Data can be written to [" + remoteAddress + "]");
//            ServletOutputStream servletOutputStream = context.getResponse().getOutputStream();
//            while (servletOutputStream.isReady()) {
//                // Make sure a write/complete operation is only done, if no other write is pending, i.e. if isReady() == true
//                // Otherwise WritePendingException is thrown.
//                if (!written) {
//                    written = true;
//                    servletOutputStream.write(data);
//                    // After this write isReady() may return false, indicating the write is not finished.
//                    // In this case onWritePossible() is invoked again as soon as the isReady() == true again,
//                    // in which case we would only complete the request.
//                } else {
//                    context.complete();
//                }
//            }
//        }
//
//        @Override
//        public void onError(Throwable throwable) {
//            Log.warn("Error writing response data to [" + remoteAddress + "]", throwable);
//            context.complete();
//        }
//    }
    
    private static class WriteListenerImpl implements WriteListener {

        private final AsyncContext context;
        private final InputStream data;
        private final String remoteAddress;

        public WriteListenerImpl(AsyncContext context, byte[] data) {
            this.context = context;
            this.data = new ByteArrayInputStream( data );
            this.remoteAddress = getRemoteAddress(context);
        }

        @Override
        public void onWritePossible() throws IOException {
            // This method may be invoked multiple times and by different threads, e.g. when writing large byte arrays.
            // Make sure a write/complete operation is only done, if no other write is pending, i.e. if isReady() == true
            // Otherwise WritePendingException is thrown.
            if( Log.isTraceEnabled() ) {
                Log.trace("Data can be written to [" + remoteAddress + "]");
            }
            synchronized ( context )
            {
                final ServletOutputStream servletOutputStream = context.getResponse().getOutputStream();
                while ( servletOutputStream.isReady() )
                {
                    final byte[] buffer = new byte[8*1024];
                    final int len = data.read( buffer );
                    if ( len < 0 )
                    {
                        // EOF - all done!
                        context.complete();
                        return;
                    }

                    // This is an async write, will never block.
                    servletOutputStream.write( buffer, 0, len );

                    // When isReady() returns false for the next iteration, the
                    // interface contract guarantees that onWritePossible() will
                    // be called once a write is possible again.
                }
            }
        }

        @Override
        public void onError(Throwable throwable) {
            if( Log.isWarnEnabled() ) {
                Log.warn("Error writing response data to [" + remoteAddress + "]", throwable);
            }
            synchronized ( context )
            {
                context.complete();
            }
        }
    }


}
